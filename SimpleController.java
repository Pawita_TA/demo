package com.krissada.demo;


import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SimpleController {
	@RequestMapping("/")
	@ResponseBody
	String home() {
		return "Hello World!";
	}

	@GetMapping("/simple")
	@ResponseBody
	String simple() {
		return "Simple";
	}

	@GetMapping(path = "/students/{student_id}")
	@ResponseBody
	public String getGrade(@PathVariable("student_id") String studentId) {
		return "The student id is " + studentId;
	}

	// @GetMapping(path = "/students/{group_id}/{student_id}") ต้องทำอย่างไร
	// 14.40 -> load MySQL กับ MySQL Workbench ให้เรียบร้อย

	@GetMapping(path = "/students2/{student_id}")
	@ResponseBody
	public String getGrade2(@PathVariable("student_id") String studentId, @RequestParam("semester") String semester) {
		return "The student id is " + studentId + " request params semester= " + semester;
	}

	// Optional ให้รับ Request Param ที่ชื่อ year เข้าไปด้วย แล้ว
	// แสดงผลออกมาให้ถูกต้อง
	
	@GetMapping(path = "/students3/{student_id}")
	public ResponseEntity<String> getGrade3(@PathVariable("student_id") 
	String studentId,
			@RequestParam("semester") String semester) {
		return new ResponseEntity<String>(studentId + " has got an A.",
				HttpStatus.NOT_FOUND);
	}
	// Optional ลองเปลี่ยน HttpStatus.OK เป็นอย่างอื่นๆ
	@PostMapping(path ="/testPost", consumes = {MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	String testPost(@RequestBody UserTestTest user) {
		return "Username " + user.getUsername() + " Password " + user.getPassword();
	}
	// Optional ถ้า post มีข้อมูลส่งมา เช่น username, password เราจะเอามาแสดงค่าได้อย่างไร
}
